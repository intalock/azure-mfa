# azure-mfa #

Azure MFA is a collection of Splunk apps which goal is to utilize the multifactor authentication logs from Microsoft Cloud Services. It contains index and props configurations that should take care of the indexing and field extraction. It also has an executive summary dashboard and scheduled searches that transforms raw Azure Multifactor Authentication logs into meaningful visual stories and alerts that should be able to help enterprises analyze and monitor user behaviour in our around the security space.

### **Individual Apps And Where They Belong** ###

#### Azure MFA UI `sh_azure_mfa_ui` ####
Azure MFA UI Splunk app is the main app housing all configurations for knowledge objects that usually interact with users, such as dashboard and alerts. This app contains the Executive Summary dashboard (the main dashboard), saved searches, search-time field extractions, and macros. This app must be installed in the search head cluster and is the only visible app among the rest.

#### Azure MFA Non-UI (Data) `sh_azure_mfa_data` ####
The same as above, this app must also be installed in the search head cluster. It contains configurations that are usually not interacting with developers all the time, e.g. authorize.conf which contains user role. Lookup table files and lookup definitions (transforms.conf) are also placed in this app.

#### Azure MFA Indexer App `idx_azure_mfa` ####
The Indexer app contains indexes.conf and props.conf (when necessary) and must be placed in the indexer cluster or slave-apps.

#### Azure MFA Forwarder App `fwd_azure_mfa` ####
This app houses the configuration for whatever input method will be used depending in your situation. The inputs.conf.example contains a sample input method i.e. TCP Listener that points to the `index = azure_mfa` and the `sourcetype = mscs:mfa` which is defined in the props.conf.example. Before enabling this app in your forwarder node, these two files must be configured and renamed by removing ".example" from the filename.

### **Instructions for Usage** ###
1. Clone the entire repository into your workspace environment (**DO NOT** directly clone the repository into `$SPLUNK_HOME/etc/apps` or `$SPLUNK_HOME/etc/slave-apps`).
	- Perform all your modifications (as mentioned below) here
2. For Azure MFA UI and Data, i.e. `sh_azure_mfa_ui` and `sh_azure_mfa_data` 
	- Review the parameter settings of the saved searches, i.e. update action whereas its current state doesn't perform any useful security actions yet. You may want to try starting with "send email" action, or Enterprise Security's "notable" action
	- Review the props.conf in `sh_azure_mfa_ui` to see if the field extractions and calculations match the structure of your raw Azure MFA logs
	- Install these two apps to your SH Cluster via your deployer. Or if you're deploying the app to a standalone Splunk env, install it directly to $SPLUNK_HOME/etc/apps directory
3. For the indexer app:
	- Modify the paramenter settings of indexes.conf to comply to your enterprise's Splunk index sizing and archiving policies
	- Feel free to add props.conf should there be any need for an indexer-residing props.conf (although this will be very unlikely)
	- Install the app to your indexer peers via the Cluster Master (or directly to the node(s) that act(s) as your indexer)
4. For the forwarder app:
	- Modify the props.conf.example to properly parse the your Azure MFA raw logs
	- Rename it to props.conf
	- Add a transforms.conf depending to your need for nullQueue or other transformative functions done in the parsing state
	- Modify the inputs.conf.example to establish your method of log ingestion
	- When necessary, add an outputs.conf to properly send the logs from _this_ forwarder to the indexer(s)
	- Install this app to your universal/heavyfowarder via a Deployment Server or directly to the node
5. In your Search Head (cluster), create a user with name "azuremfa-ssrunner-user" and assign the role "azuremfa-ssrunner-role" to this user


##### For read/write access requests: #####
- Contact the following repo admin should you wish to collaborate to this project
	
	- Greg Ford (<greg.ford@intalock.com.au>)
	- Mark Hill (<mark.hill@intalock.com.au>)
	- Callum Bevan (<callum.bevan@intalock.com.au>)
	- Daniel Astillero (<daniel.astillero@intalock.com.au>)